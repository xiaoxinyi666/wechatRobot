import werobot
import os
from tuling import visit_tuling
from baidu import visit_baidu

token = os.getenv("token", "XTiw5QAnhO3NOPiYS5AUyQvxOdKI")
robot = werobot.WeRoBot(token=token)

@robot.text
def home(message,session):
    if "百度百科" in message.content:
        session['百度百科']=True
        return "欢迎使用百度百科,例如；长沙学院的地址是什么？ 输入“退出” 退出词条"
    if session['百度百科']:
        if "退出" not in message.content:
            return visit_baidu(message.content)
        else:
            session['百度百科'] = False
            return "退出百度百科"
    return visit_tuling(message.content)

robot.config['HOST'] = '0.0.0.0'
robot.config['PORT'] = 80
robot.run()
